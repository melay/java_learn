package Praktikum1;

import static org.junit.Assert.*;

import org.junit.Test;

public class QuadratischeFunktionTest {
	final private static double DOUBLE_EPS = 0.000001; // Precision in comparison of double numbers

	/**
	 * Test if first constructor initialises correctly.
	 */
	@Test
	public void test01() {

		double a0 = -2.0;
		double a1 = -2.0;

		while (a0 <= 2.0) {
			while (a1 <= 2.0) {
				QuadratischeFunktion qf = new QuadratischeFunktion(a1, a0);
				assertEquals("Erster Konstruktor falsch (falscher Koeffizient)", a0, qf.getA(0), DOUBLE_EPS);
				assertEquals("Erster Konstruktor falsch (falscher Koeffizient)", a1, qf.getA(1), DOUBLE_EPS);
				assertEquals("Erster Konstruktor falsch (falscher Koeffizient)", 1.0, qf.getA(2), DOUBLE_EPS);
				assertEquals("Erster Konstruktor falsch (falscher Grad)", 2, qf.getGrad());

				a1 += 0.1;
			}
			a0 += 0.1;
			a1 = -2.0;
		}
	}

	/**
	 * Test if second constructor initialises correctly.
	 */
	@Test
	public void test02() {

		double a0 = -2.0;
		double a1 = -2.0;
		double a2 = -2.0;

		while (a0 <= 2.0) {
			while (a1 <= 2.0) {
				while (a2 <= 2.0) {
					QuadratischeFunktion qf = new QuadratischeFunktion(a2, a1, a0);
					assertEquals("Erster Konstruktor falsch (falscher Koeffizient)", a0, qf.getA(0), DOUBLE_EPS);
					assertEquals("Erster Konstruktor falsch (falscher Koeffizient)", a1, qf.getA(1), DOUBLE_EPS);
					assertEquals("Erster Konstruktor falsch (falscher Koeffizient)", a2, qf.getA(2), DOUBLE_EPS);
					assertEquals("Erster Konstruktor falsch (falscher Grad)", 2, qf.getGrad());

					a2 += 0.1;
				}
				a1 += 0.1;
				a2 = -2.0;
			}
			a0 += 0.1;
			a1 = -2.0;
		}
	}

	/**
	 * Test if Method nullstellen works correctly.
	 */
	@Test
	public void test03() {

		double a0 = -2.0;
		double a1 = -2.0;
		double a2 = -2.0;

		for (int b0 = -20; b0 <= 20; b0++) {
			a0 = b0 / 10.0;
			for (int b1 = -20; b1 <= 20; b1++) {
				a1 = b1 / 10.0;
				for (int b2 = -20; b2 <= 20; b2++) {
					a2 = b2 / 10.0;

					if (b2 != 0) { // otherwise it is not a quadratic function
						QuadratischeFunktion qf = new QuadratischeFunktion(a2, a1, a0);

						Komplex[] nullstellen = qf.getNullstellen();

						assertEquals("Methode nullstellen falsch (nicht 2 Nullstellen)", 2, nullstellen.length);

						for (int i = 0; i < nullstellen.length; i++) {
							Komplex erg = new Komplex(a0, 0); // erg = a0;

							Komplex n1 = new Komplex(nullstellen[i].getReal(), nullstellen[i].getImaginaer()); // copy.
							n1.multipliziere(new Komplex(a1, 0)); // *a1
							erg.addiere(n1); // erg = a1*x+a0

							Komplex n2 = new Komplex(nullstellen[i].getReal(), nullstellen[i].getImaginaer()); // copy.
							n2.multipliziere(nullstellen[i]); //
							n2.multipliziere(new Komplex(a2, 0)); // *a2
							erg.addiere(n2); // erg = a2*x*x+a1*x+a0

							if (erg.getBetrag() > DOUBLE_EPS || -erg.getBetrag() > DOUBLE_EPS) {
								System.out.println("Problem bei:");
								qf.ausgabe();
								System.out.println("Nullstelle:");
								nullstellen[i].drucke();

							}

							assertEquals("Methode nullstellen falsch (falsche Nullstelle)", 0.0, erg.getBetrag(),
									DOUBLE_EPS);

						}
					}
				}
			}
		}
	}
}


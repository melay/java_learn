package Praktikum1;

import java.util.Arrays;

public class QuadratischeFunktion extends Polynom{
	
	public QuadratischeFunktion( double a0 , double a1, double a2) {
		super(a0,a1,a2);
	}
	
	public QuadratischeFunktion( double a0 , double a1) {
		super(1.0,a0,a1);
	}
	
	public double getA(int n) {
		return koeffizienten[koeffizienten.length-n-1];
	}
	
	public Komplex []  getNullstellen(){
		double p=koeffizienten[1]/koeffizienten[0];
		double q=koeffizienten[2]/koeffizienten[0];
    
		Komplex [] x = new Komplex[2];
    
		double rad = ((p*p)/4.0)-q;
    
		if(rad>=0){
			double x1 = ((-1.0*p)/2)-Math.sqrt(rad);
			double x2 = ((-1.0*p)/2)+Math.sqrt(rad);
			x[0]=new Komplex(x1,rad);
			x[1]=new Komplex(x2,rad);
		}else{
			double x1 = ((-1.0*p)/2);
			double x2 = ((-1.0*p)/2);
			x[0]=new Komplex(x1,rad);
			x[1]=new Komplex(x2,rad);
		}
		return x;
	}
		
	public static void main(String []args){
		QuadratischeFunktion p1 = new QuadratischeFunktion (1,2,3); // 2x+1
		System.out.println(Arrays.toString(p1.getNullstellen()));	
		System.out.println(p1.getA(0));
		
	}
}


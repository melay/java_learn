package Praktikum1;

import static org.junit.Assert.*;

import org.junit.Test;

public class T2 {
	
	private final static double DOUBLE_EPS = 0.000001; 
	/** Test if default constructor initialises correctly.
	 */
	@Test
	public void test01() {
		double re=-2.0, im=2.0;
		
		while (re<=2.0) {
			Komplex k = new Komplex(re,im);
		
			assertEquals(re, k.getReal(),DOUBLE_EPS);
			assertEquals(im, k.getImaginaer(), DOUBLE_EPS);
			
			re+=0.1;
			im-=0.1;
		}
	}


	/** Test for addition.
	 */
	@Test
	public void test02() {
		double reA=-2.0, imA=2.0;
		double reB=4.5678, imB=-2.3456;
		
		while (reA<=2.0) {
			Komplex k = new Komplex(reA,imA);
			Komplex k2 = new Komplex(reB,imB);
			
			k.addiere(k2);
			
			assertEquals(reA+reB, k.getReal(), DOUBLE_EPS);
			assertEquals(imA+imB, k.getImaginaer(), DOUBLE_EPS);
			
			reA+=0.1;
			imA-=0.1;
			reB-=0.1;
			imB+=0.1;
		}
	}

	/** Test for multiplication.
	 */
	@Test
	public void test03() {
		double reA=-2.0, imA=2.0;
		double reB=4.5678, imB=-2.3456;
		
		while (reA<=2.0) {
			Komplex k = new Komplex(reA,imA);
			Komplex k2 = new Komplex(reB,imB);
			
			k.multipliziere(k2);
			
			assertEquals(reA*reB-imA*imB, k.getReal(), DOUBLE_EPS);
			assertEquals(imA*reB+reA*imB, k.getImaginaer(), DOUBLE_EPS);
			
			reA+=0.1;
			imA-=0.1;
			reB-=0.1;
			imB+=0.1;
		}
	}
	/** Test for polar coordinates
	 */
	@Test
	public void test04() {
		double betrag=1.0;
		double winkel=0.0;
		
		while (betrag<=2.0) {
			Komplex k = new Komplex(0,0);
			k.setzePolar(winkel, betrag);
			Komplex k2 = new Komplex(0,0);
			k2.setzePolar(Math.PI*2-winkel, betrag);
			
			k.multipliziere(k2);
			
			assertEquals(betrag*betrag, k.getReal(), DOUBLE_EPS);
			assertEquals(0, k.getImaginaer(), DOUBLE_EPS);
			
			betrag+=0.01;
			winkel+=Math.PI*2/100;
		}
	}

	/** Test for roots
	 */
	@Test
	public void test05() {
		double reA=-2.0;
		double imA=2.0;
		
		
		while (reA<=2.0) {
			Komplex k = new Komplex(reA,imA);
			
			for (int n=2; n<7; n++) {
				for (int j=1; j<=n; j++) {
					Komplex k2 = new Komplex(reA,imA);
					Komplex k3 = new Komplex(reA,imA);
					k2.wurzel(n,j);
					k3.wurzel(n,j);
					for (int i=1; i<n;i++) k3.multipliziere(k2);
					
					
					assertEquals(k.getReal(), k3.getReal(), DOUBLE_EPS);
					assertEquals(k.getImaginaer(), k3.getImaginaer(), DOUBLE_EPS);
					
					
				}
			}

			reA+=0.1;
			imA-=0.1;
		
			
		}
	}
	/** Test for exponent
	 */
	@Test
	public void test06() {
		double reA=-2.0;
		double imA=2.0;
		
		
		while (reA<=2.0) {
			
			Komplex k = new Komplex(reA,imA);
			
			for (int n=2; n<7; n++) {
				for (int j=1; j<=n; j++) {
				Komplex k2 = new Komplex(reA,imA);
				k2.wurzel(n,j);
				k2.hoch(n); // Rolle rueckwaerts
					
				assertEquals(k.getReal(), k2.getReal(), DOUBLE_EPS);
				assertEquals(k.getImaginaer(), k2.getImaginaer(), DOUBLE_EPS);
					
					
				}
			}

			reA+=0.1;
			imA-=0.1;
		
			
		}
	}


}

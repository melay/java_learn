package Praktikum1;

import java.util.Arrays;

public class Polynom {
	public double[] koeffizienten;
	
	public Polynom () {
	}
	
	public Polynom ( double a0 ) {
		koeffizienten = new double [1];
		koeffizienten[0] = a0;
	}
	public Polynom ( double a0 , double a1 ) {
		koeffizienten = new double [2];
		koeffizienten[0] = a0;
		koeffizienten[1] = a1;
	}
	public Polynom ( double a0 , double a1 , double a2 ) {
		koeffizienten = new double [3];
		koeffizienten[0] = a0;
		koeffizienten[1] = a1;
		koeffizienten[2] = a2;
		
	}
	public Polynom ( double [] koeffizienten ) {
		this.koeffizienten = koeffizienten;
	}
	
	public void ausgabe() {
		System.out.print("f(x) = "+koeffizienten[0]);
		for(int i=1; i<koeffizienten.length; i++) {
			if (koeffizienten[i]>=0) {
				System.out.print("+");
			}/*else {
				System.out.print("-");
			}*/
			System.out.print(koeffizienten[i]+"*x^"+i);
		}
		System.out.println("");
	}
	
	public double auswerte(double x) {
		double erg;
		erg=koeffizienten[0];
		for(int i=1; i<koeffizienten.length; i++) {
			erg+=koeffizienten[i]*Math.pow(x, i);
		}
		return erg;
	}
	
	public void addiere(Polynom p) {
		int max_len = Math.max(koeffizienten.length, p.koeffizienten.length);
		double [] temp_cos = koeffizienten;
		koeffizienten = new double [max_len];
		for(int i=0; i<max_len; i++) {
			if(i<temp_cos.length && i<p.koeffizienten.length){	
				koeffizienten[i]=temp_cos[i]+p.koeffizienten[i];
			}else if(i<temp_cos.length){
				koeffizienten[i]=temp_cos[i];
			}else if(i<p.koeffizienten.length){
				koeffizienten[i]=p.koeffizienten[i];
			}
		}
	}
	
	public void subtrahiere(Polynom p) {
		int max_len = Math.max(koeffizienten.length, p.koeffizienten.length);
		double [] temp_cos = koeffizienten;
		koeffizienten = new double [max_len];
		for(int i=0; i<max_len; i++) {
			if(i<temp_cos.length && i<p.koeffizienten.length){	
				koeffizienten[i]=temp_cos[i]-p.koeffizienten[i];
			}else if(i<temp_cos.length){
				koeffizienten[i]=temp_cos[i];
			}else if(i<p.koeffizienten.length){
				koeffizienten[i]=-1*p.koeffizienten[i];
			}
		}
	}
	
	public void multipliziere(Polynom p) {
      double[] produkt= new double[koeffizienten.length + p.koeffizienten.length];
      for(int i=0;i<p.koeffizienten.length;i++){ 
        for(int j=0;j<koeffizienten.length;j++){
          produkt[i+j]+=p.koeffizienten[i]*koeffizienten[j];
        }
      }
      koeffizienten = new double [produkt.length];
      koeffizienten = produkt;
	}
	
	public int getGrad() {
		int erg=0;
		
		for(int i=0;i<koeffizienten.length;i++) {
			if(koeffizienten[i]!=0.0) {
				erg=i;
				
			}
		}
		return erg;
	}
	
	public Polynom ableitung() {
		double[] erg = new double[koeffizienten.length-1];
		for(int i=1; i<koeffizienten.length; i++) {
			erg[i-1]=koeffizienten[i]*i;
		}
		Polynom ret = new Polynom(erg);
		return ret;
	}
	
	public void negiere () {
		for(int i=0; i<koeffizienten.length; i++) {
			koeffizienten[i]=koeffizienten[i]*-1.0;
		}
	}
	

	public double getA(int n) {
		if(n>=koeffizienten.length) {
			return 0;
		}else {
			return koeffizienten[n];
		}
	}
	
	public static void main(String []args){
		Polynom p5;
		Polynom p1 = new Polynom (1); // 2x+1
		Polynom p2 = new Polynom (new double [] {1 ,2, 3 });
		Polynom p3 = new Polynom (new double [] {1,1,3,4});
		Polynom p4 = new Polynom (new double [] {1,2,5,3});
		p3.ausgabe();
		p3.multipliziere(p4);
		p3.ausgabe();
		
		/*System .out. println (" P1 : " );
		p1. ausgabe ();
		System .out. println (" An der Stelle x =3 ist p ( x )= "+p1. auswerte (3));
		System .out. println (" Der Grad ist : " +p1. getGrad ());
		
		System .out. println (" P2 : " );
		p2. ausgabe ();
		System .out. println (" An der Stelle x =3 ist p ( x )= "+p2. auswerte (3));
		System .out. println (" Der Grad ist : " +p2. getGrad ());*/
	}
}

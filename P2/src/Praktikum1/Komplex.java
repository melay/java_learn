package Praktikum1;

public class Komplex{
	public double local_re, local_im;
	public double local_winkel, local_betrag;
	
	public Komplex (double re, double im){
		local_re=re;
		local_im=im;
	}
	
	public void setzePolar ( double winkel , double betrag ) {
		local_re = betrag * Math.cos(winkel);
		local_im = betrag * Math.sin(winkel);
		local_winkel = winkel;
		local_betrag = betrag;
	}
	
	public void drucke(){
		if(local_im<0){
			System.out.println(local_re+""+local_im+"*i");
		}else{
			System.out.println(local_re+"+"+local_im+"*i");
		}
	}
	
	public void wurzel (int n) {
		setPolar();
		local_re=Math.sqrt(local_betrag)*Math.cos(0.5*local_winkel);
		local_im=Math.sqrt(local_betrag)*Math.sin(0.5*local_winkel);

	}
	
	public void wurzel (int n, int k) {
		setPolar();
		local_re = Math.pow(local_betrag, (double) (1.0/n)) * Math.cos((local_winkel+2*Math.PI*k)/n);
		local_im = Math.pow(local_betrag, (double) (1.0/n)) * Math.sin((local_winkel+2*Math.PI*k)/n);
	}
	
	public void addiere(Komplex z){
		local_re += z.local_re;
		local_im += z.local_im;
	}
	
	public void setPolar() {
		local_betrag=Math.sqrt((local_re*local_re)+(local_im*local_im));
		local_winkel=Math.atan2(local_im,local_re);
	}
	
	public void hoch ( int n) {
		setPolar();
		local_re = Math.pow(local_betrag,n)*Math.cos(local_winkel*n);
		local_im = Math.pow(local_betrag,n)*Math.sin(local_winkel*n);
	}
	
	public void multipliziere(Komplex z){	
	    double x1=local_re, x2=z.local_re;
		double y1=local_im, y2=z.local_im;
		local_re = ((x1*x2) - (y1*y2));
		local_im = ((x1*y2) + (x2*y1));
	}
	
	public double getBetrag () {
		return local_betrag;
	}
	
	public double getReal () {
		return local_re;
	}
	
	public double getImaginaer () {
		return local_im;
	}
	
	public static void main(String []args){
		Komplex a = new Komplex(2,2),
		b = new Komplex(3,1),
		i = new Komplex(0,1);
		a.drucke();	
		a.addiere(b);
		a.drucke();
		a.addiere(i);
		a.drucke();
		a.multipliziere(i);
		a.drucke();
		a.multipliziere(b);
		a.drucke();
		a.multipliziere(i);
		a.drucke();		
	}
}

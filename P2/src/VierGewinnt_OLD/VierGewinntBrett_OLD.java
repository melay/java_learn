package VierGewinnt;

public class VierGewinntBrett {
	private int[] feld;
    
	public VierGewinntBrett() {
    	feld = new int[42];  
    }
    
    // getter und setter
    public int getFeld(int pos) {
    	return feld[pos];
    }
    public void setFeld(int pos, int wert) {
    	feld[pos] = wert;
    }
    public int getSpalte(int pos) {
    	int spalte=0;
    	for (int i=1;i<7;i++) {
    		spalte=((i-1)*7)+pos-1;
    		if(feld[spalte]!=0) {
    			return spalte=((i-2)*7)+pos-1;
    		}		
    	}
    	return spalte;
    }


    public void setSpalte(int pos, int wert) {
    	int position = getSpalte(pos);
    	feld[position] = wert;
    }
    

    public void printBrett() {
    String headFoot = new String("+-+-+-+-+-+-+-+");
	System.out.println(headFoot);
	for (int i=0; i<42; i++) {
	    System.out.print("|");
	    
	    switch(feld[i]) { 
	    	case 0: System.out.print(" "); break;
	    	case 1: System.out.print("o"); break;
	    	case 2: System.out.print("x"); break;
	    	default:  System.out.print("?");
	    }

	    
	    if (i %7==6) { // neue Zeile?
	    	System.out.println("|");
	    	System.out.println(headFoot);
	    }
	}
	System.out.println();
    }
}

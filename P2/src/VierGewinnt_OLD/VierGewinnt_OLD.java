package VierGewinnt;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class VierGewinnt extends Spiel {
	public VierGewinntBrett brett;
	private int werIstDran;
	private static BufferedReader stdin;
	
	public VierGewinnt() {
		werIstDran=1; // Weiss faengt an
    }
	
    public VierGewinntBrett neuesBrett() {
    	brett = new VierGewinntBrett();
    	return brett;
    }
    
    public void neuesSpiel(VierGewinntBrett b) {
	// Feld leeren: alles auf 0
	for (int i=0; i<9; i++) 
	    b.setFeld(i,0); 
    }
	
    public void macheZug(VierGewinntBrett b) {
    	System.out.println(" Spieler " + werIstDran+", "+"geben Sie eine Spalte ein (1-7) :");
    	Boolean fertig = false;
    	do {
    		try {
    			String line = stdin.readLine();
				int index = Integer.parseInt(line);
				if(index>0 && index<=7 && b.getSpalte(index)>=0) {
					b.setSpalte(index,werIstDran);
					b.printBrett();
					fertig = true;
					werIstDran = 3-werIstDran;
				}else {
					System.out.println("W�hlen Sie ein anderes Feld"+b.getSpalte(index));
				}
    		}catch (Exception ex) {
	    			System.out.println("Fehlerhafte Eingabe");
	    	}
    	}while (!fertig);
    } 

    public Boolean istBeendet(VierGewinntBrett b) {
    	int anzahlLeereFelder=0;
    	for (int i=0; i<42; i++) 
    		if (b.getFeld(i)==0) anzahlLeereFelder++;
    	if (anzahlLeereFelder==0) return true;
    	
    	int counter_h=0;
    	int counter_v=0;
    	int counter_d1=0;
    	int counter_d2=0;
    	
    	//Diagonal-Right
    	for(int i=0;i<=14;i++) { //14 is the last occurrence of 4 possible matches
    		int j=i;
    		do {
    			if(b.getFeld(j)==b.getFeld(j+8) && b.getFeld(j)!=0) {
    				counter_d1++;
    			}else {
    				counter_d1=0;
    			}
    			if(counter_d1==3) {
    				System.out.println("Diag-Win");
        			return true;
    			}
    			j=j+8;
    		}while(j%7!=0 && j<=27);
    	}
    	//Diagonal-Left
    	for(int i=32;i>=0;i--) { //38 is the first occurrence where 4 can be build up
    		int j=i;
    		do {
    			if(b.getFeld(j)==b.getFeld(j+6) && b.getFeld(j)!=0) {
    				counter_d2++;
    			}else {
    				counter_d2=0;
    			}
    			if(counter_d2==3) {
    				System.out.println("Diag-Win");
        			return true;
    			}
    			j=j-6;
    		}while(j>=3);
    	}

    	//Horizontal
    	for(int i=0;i<=40;i++) {
    		if(b.getFeld(i)==b.getFeld(i+1) && b.getFeld(i)!=0 && (i+1)%7!=0) {
    			counter_h++;
    		}else {
    			counter_h=0;
    		}
    		if(counter_h==3) {
    			System.out.println("Horz-Win");
    			return true;
    		}
    	}	
    	//Vertikal
    	for(int j=0;j<=41;j++) {
    		int f1=((j%6)*7)+ (int) Math.floor(j/6);
    		int f2=(((j+1)%6)*7)+ (int) Math.floor((j+1)/6); //j+1
  		    if(b.getFeld(f1) == b.getFeld(f2) && b.getFeld(f1) != 0 && (j+1)%6!=0) {
  		    	counter_v++;
  		    }else {
  			   counter_v=0;
  		    }
  		    
    		if(counter_v==3) {
    			System.out.println("Vert-Win");
    			return true;
    		}
    	}   	
    	//ELse
    	return false;
    }
    
	public static void main(String[] args) {
		VierGewinnt vg = new VierGewinnt();
		stdin = new BufferedReader(new InputStreamReader(System.in));
		vg.neuesBrett();
		vg.spielen();
		
	}
}

package VierGewinnt;

public abstract class Spiel {
    public VierGewinntBrett brett;
    public abstract VierGewinntBrett neuesBrett();
    public abstract void neuesSpiel(VierGewinntBrett b);
    public abstract void macheZug(VierGewinntBrett b);
    public abstract Boolean istBeendet(VierGewinntBrett b);

    // Jetzt koennen wir schon spielen:
    public void spielen() {
    	brett = neuesBrett();  
    	neuesSpiel(brett);
    	do { // solange ziehen, bis beendet
    	    macheZug(brett); 
    	} while (! istBeendet(brett)); 

    }
}

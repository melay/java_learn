package Praktikum2;
import TTT.src.Brett;
// needed by VierGewinntFrame
public interface VierGewinntCtrl {

	// method to inform the Ctrl that button "i" has been pressed
	void columnSelected(int i);
	// ask Ctrl whether a move is legal
	boolean isLegalMove(int i);
	// ask Ctrl whether game is finished
	boolean isFinished();
	// method to inform the Ctrl that a new game shall be started
	public void newGame();
	// retrieve the current board from Ctrl
	Brett getBoard(); 

}

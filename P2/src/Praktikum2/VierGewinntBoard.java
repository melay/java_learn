package Praktikum2;
import TTT.src.*;
public interface VierGewinntBoard {
	// retrieve information about contents of board
	int getField(int column, int row);
	// 0: empty
	// 1: Player 1 owns that field
	// 2: Player 2 owns that field
}

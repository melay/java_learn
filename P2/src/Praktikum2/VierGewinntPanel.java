package Praktikum2;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class VierGewinntPanel extends JPanel {

	private VierGewinntBoard board;

	public void paint(Graphics g) {
		
		// find out the dimension of the panel
		Dimension d = getSize();
		int w = (int) d.getWidth();
		int h = (int) d.getHeight();
		
		// compute the size of the squares
		int width = w/7; // 7 columns
		int height = Math.min(h/6,width); // 6 rows, but not higher than width
		
		int border = width/5; // distance between border and red/yellow chip
		
		
		g.setColor(Color.blue);
		g.fillRect(0, 0, width*7, height*6); // making it blue first
		
		g.setColor(Color.white);
		for (int i=1; i<7; i++) { // marking the squares
			g.drawLine(i*width, 0, i*width, 6*height);
		}
		for (int i=1; i<6; i++) {
			g.drawLine(0,i*height,7*width, i*height);
		}
		// now we draw the circles...
		for (int row=0;row<6;row++) {
			for (int column=0; column<7; column++) {
				switch(board.getField(column,row)) {
				case 0: // not used yet -> white
					g.setColor(Color.white);
					break;
				case 1: // player 1 is yellow
					g.setColor(Color.yellow);
					break;
				case 2:  // player 2 is red
					g.setColor(Color.red);
					break;
					
				} 
				// now draw the circle (in general an oval)
				g.fillOval(column*width+border, (5-row)*height+border, width-2*border, height-2*border);
			
			}
		}
		
	}
	// setting the board to draw
	public void setBoard(VierGewinntBoard board) {
		this.board = board;
	}
}

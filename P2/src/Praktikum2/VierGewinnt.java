package Praktikum2;
import java.io.BufferedReader;
import java.io.InputStreamReader;


import TTT.src.*;

public class VierGewinnt extends Spiel{
	
	private static BufferedReader stdin;
	private int werIstDran;
	public Brett brett;
	
	
	public VierGewinnt() {
		werIstDran=1;
	}
    public Brett neuesBrett() {
    	brett = new VierGewinntBrett();
    	return brett;
    }
	
    public void neuesSpiel(Brett b) {
	for (int i=0; i<42; i++)
			b.setFeld(i,0); 
    }
    
    public void macheZug(Brett b) {
    	System.out.println(" Spieler " + werIstDran+", "+"geben Sie einen Zug ein (0-8) :");
    	Boolean fertig = false;
    	do {
    		try {
    			String line = stdin.readLine();
    			int index = Integer.parseInt(line);
		
    			if (index<7 && index>=0) {
    				//b.setFeld(index,werIstDran);
    		    	int spalte=0;
    		    	for (int i=1;i<7;i++) {
    		    		spalte=((i-1)*7)+index-1;
    		    		if(b.getFeld(spalte)!=0) {
    		    			spalte=((i-2)*7)+index-1;
    		    			break;
    		    		}		
    		    	}
    		    	b.setFeld(spalte, werIstDran);
    				b.printBrett();
    				fertig = true;
    				werIstDran = 3-werIstDran;
    			}
		else { 
		    System.out.println("W�hlen Sie ein anderes Feld");
		} 
	    }
	    catch (Exception ex) {
		System.out.println("Fehlerhafte Eingabe");
	    }
	    
	} while (!fertig);
    }
    
    public Boolean istBeendet(Brett b) {
    	int anzahlLeereFelder=0;
    	for (int i=0; i<42; i++) 
    		if (b.getFeld(i)==0) anzahlLeereFelder++;
    	if (anzahlLeereFelder==0) return true;
    	
    	int counter_h=0;
    	int counter_v=0;
    	int counter_d1=0;
    	int counter_d2=0;
    	
    	//Diagonal-Right
    	for(int i=0;i<=14;i++) { //14 is the last occurrence of 4 possible matches
    		int j=i;
    		do {
    			if(b.getFeld(j)==b.getFeld(j+8) && b.getFeld(j)!=0) {
    				counter_d1++;
    			}else {
    				counter_d1=0;
    			}
    			if(counter_d1==3) {
    				System.out.println("Diag-Win");
        			return true;
    			}
    			j=j+8;
    		}while(j%7!=0 && j<=27);
    	}
    	
    	//Diagonal-Left
    	for(int i=32;i>=0;i--) { //38 is the first occurrence where 4 can be build up
    		int j=i;
    		do {
    			if(b.getFeld(j)==b.getFeld(j+6) && b.getFeld(j)!=0) {
    				counter_d2++;
    			}else {
    				counter_d2=0;
    			}
    			if(counter_d2==3) {
    				System.out.println("Diag-Win");
        			return true;
    			}
    			j=j-6;
    		}while(j>=3);
    	}
    	//Horizontal
    	for(int i=0;i<=40;i++) {
    		if(b.getFeld(i)==b.getFeld(i+1) && b.getFeld(i)!=0 && (i+1)%7!=0) {
    			counter_h++;
    		}else {
    			counter_h=0;
    		}
    		if(counter_h==3) {
    			System.out.println("Horz-Win");
    			return true;
    		}
    	}	
    	//Vertikal
    	for(int j=0;j<=41;j++) {
    		int f1=((j%6)*7)+ (int) Math.floor(j/6);
    		int f2=(((j+1)%6)*7)+ (int) Math.floor((j+1)/6); //j+1
  		    if(b.getFeld(f1) == b.getFeld(f2) && b.getFeld(f1) != 0 && (j+1)%6!=0) {
  		    	counter_v++;
  		    }else {
  			   counter_v=0;
  		    }
  		    
    		if(counter_v==3) {
    			System.out.println("Vert-Win");
    			return true;
    		}
    	} 
    	
    	return false;
    }
    
    public static void main(String[] args) {
    	stdin = new BufferedReader(new InputStreamReader(System.in)); 
    	VierGewinnt vg = new VierGewinnt();
    	//vg.spielen();
    	vg.spielen();
	}

}

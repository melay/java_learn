package Praktikum2;

import TTT.src.Brett;

public class VierGewinntSpiel implements VierGewinntCtrl{
	// method to inform the Ctrl that button "i" has been pressed
	public void columnSelected(int i) {
		
	}
	// ask Ctrl whether a move is legal
	public boolean isLegalMove(int i) {
		return false;
	}
	// ask Ctrl whether game is finished
	public boolean isFinished() {
		return false;
	}
	// method to inform the Ctrl that a new game shall be started
	public void newGame() {
		new VierGewinntCtrl 
		VierGewinntFrame(this);
	}
	// retrieve the current board from Ctrl
	public Brett getBoard() {
    	return brett;
	}

}

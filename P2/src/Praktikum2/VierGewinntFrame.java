package Praktikum2;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class VierGewinntFrame extends JFrame 
implements ActionListener 
{

	private VierGewinntPanel panel ;
	private JPanel contentPane ;
	private JLabel message ;

	private JButton[] columnButtons; 
	private VierGewinntCtrl ctrl;
	private JButton newGameButton;

	public VierGewinntFrame(VierGewinntCtrl ctrl) {
		super("Vier Gewinnt"); // Title of the window
		this.ctrl=ctrl;
		


		// construction of the window
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5)); // distances in all directions
		contentPane.setLayout(new GridBagLayout()); // Layout-Style defined
		GridBagConstraints c = new GridBagConstraints(); // Positioning constraints of elements
		setContentPane(contentPane); 

		// we need seven buttons
		columnButtons = new JButton[7];

		//c.fill = GridBagConstraints.VERTICAL;
		c.gridy=0; // first row
		c.gridwidth=1; // 1 column
		c.gridheight=1;
		c.weightx=1.0/7; // if window resizes -> resize width
		c.weighty=0	; // but don't resize height
		c.fill=GridBagConstraints.BOTH; // fill whole space given
		for (int i=0; i<7; i++) {
			columnButtons[i] = new JButton(Integer.toString(i+1)); // Button "i+1"
			columnButtons[i].setMinimumSize(new Dimension(100,50));
			columnButtons[i].addActionListener((ActionListener) this); // we listen to actions 
			c.gridx=i; // column i
			contentPane.add(columnButtons[i],c); // add to panel
		}


		panel = new VierGewinntPanel();  // pure 4-in-a-row-panel
		panel.setBoard((VierGewinntBoard) ctrl.getBoard());
		panel.setMinimumSize(new Dimension(700,600)); // should be large		
		c.gridx=0;
		c.gridy=1; // below buttons
		c.gridwidth=7; // if resizing happens -> resize this panel appropriately
		c.gridheight=6;
		c.weightx=1;
		c.weighty=1;
		contentPane.add(panel, c);

		message = new JLabel("--"); // message below board
		message.setMinimumSize(new Dimension(500,50));
		Font schrift = new Font("SansSerif", Font.BOLD, 36); // I am short sighted
		message.setFont(schrift);

		c.gridx=0; 
		c.gridy=7; // last row
		c.gridwidth=5; // 5 columns
		c.gridheight=1;
		c.weightx=5.0/7; // resizing -> only width changes
		c.weighty=0;
		contentPane.add(message, c);
		
		newGameButton = new JButton("Neues Spiel"); // restart button
		newGameButton.setMinimumSize(new Dimension(200,50));
		newGameButton.addActionListener((ActionListener) this); // we listen to actions
		c.gridx=5; // right bottom corner 
		c.gridy=7;
		c.gridwidth=2; // 2 columns
		c.gridheight=1;
		c.weightx=2.0/7; // resizing -> only width changes
		c.weighty=0;
		contentPane.add(newGameButton,c);
		newGameButton.setEnabled(false); // initially not enabled!


		setMinimumSize(new Dimension(700,700)); // not smaller than this		
		setDefaultCloseOperation(EXIT_ON_CLOSE); // close-button behaviour
		setSize(700, 700); // start with this
		setVisible(true);

	}
	
	// changes the message in the left bottom corner
	public void setMeldung(String text) {
		panel.setBoard((VierGewinntBoard) ctrl.getBoard());
		message.setText(text);
		revalidate(); // repaint the whole window
		repaint();


	}
	/*
	// change the board that is shown
	public void setBrett(VierGewinntBrett board) {
		panel.setBrett(board);
		revalidate();// repaint the whole window
		repaint();
	}
*/
	// here we define what happens if a button is pressed
	// In this case this methods is called since we are
	// an "actionListener" and listen to button actions
	@Override
	public void actionPerformed(ActionEvent e) {
		// find out which button was pressed
		for (int i=0; i<7; i++)
			if( e.getSource()== columnButtons[i]) {
				ctrl.columnSelected(i); // ok, it was column i
			}	
		if (e.getSource() == newGameButton) 	
			ctrl.newGame(); // Ok, User wants new game
		
		
		if (!ctrl.isFinished()) { // if it is not over
			for (int i=0; i<7; i++) { // enabled the buttons with legal moves
				columnButtons[i].setEnabled( ctrl.isLegalMove(i));
			}	
			newGameButton.setEnabled(false); // new game not allowed yet -> you have to play on!
		}
		else { // game over
			for (int i=0; i<7; i++) {
				columnButtons[i].setEnabled(false); // it is over, no move possible
			}	
			newGameButton.setEnabled(true); // now enable restart button
		}
	}


}



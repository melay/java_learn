package Praktikum2_A3;

enum Prefix{
	Exa(18),Peta(15),Tera(12),Giga(9),Mega(6),Kilo(3),None(0),Milli(-3),Mikro(-6),Nano(-9),Piko(-12),Femto(-15),Atto(-18);
	private final int exponent;
	private Prefix(int exponent){
		this.exponent=exponent;
	}
	
	public int getExponent(){
		return exponent;
	}
	
	public String toString(){
		String outString = "";
		switch(this) {
			case Exa:  outString = "E";
						break;
			case Peta:  outString = "P";
						break;
			case Tera:  outString = "T";
						break;
			case Giga:  outString = "G";
						break;
			case Mega:  outString = "M";
						break;
			case Kilo:  outString = "K";
						break;
			case None:  outString = "";
						break;
			case Milli:  outString = "m";
						break;
			case Mikro:  outString = "�";
						break;
			case Nano:  outString = "n";
						break;		
			case Piko:  outString = "p";
						break;
			case Femto:  outString = "f";
						break;
			case Atto:  outString = "a";
						break;				
			default:	outString = "Error";
						break;
		}
		return outString;
	}
	
	public static Prefix getPrefix(int exponent){
		for(Prefix a : Prefix.values()){
			if(a.exponent == exponent) return a;
		}
      throw new PrefixOutOfRangeException(exponent); //To ammend
	}

	public Prefix multiply(Prefix aPrefix){
		return getPrefix(this.getExponent()+aPrefix.getExponent());
	}
	
	public Prefix divide(Prefix aPrefix){
		return getPrefix(this.getExponent()-aPrefix.getExponent());
	}
	
  // arguments are passed using the text field below this editor
  public static void main(String[] args)
  {
    Prefix a1 = Prefix.Kilo;
    System.out.println(a1.toString());
	Prefix a2 = Prefix.getPrefix(9);
	System.out.println(a2.toString());
	System.out.println(a1.multiply(a2));
  }
}

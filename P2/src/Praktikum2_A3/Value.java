package Praktikum2_A3;

public class Value {

	public double value;
	public Unit aUnit;
	public Prefix aPrefix;
	
	Value(double value, Prefix aPrefix, Unit aUnit){
		this.value=value;
		this.aUnit=aUnit;
		this.aPrefix=aPrefix;
	}
	
	public Unit getUnit() {
		return aUnit;
	}
	
	public void setUnit(Unit aUnit) {
		this.aUnit = aUnit;
	}
	
	public double getValue() {
		return value;
	}
	
	public void setValue(double value) {
		this.value = value;
	}
	
	public Prefix getPrefix() {
		return aPrefix;
	}
	
	public void setPrefix(Prefix aPrefix) {
		this.aPrefix = aPrefix;
	}
	
	public String toString() {
		return value+" "+aPrefix.toString()+aUnit.toString();
	}
	
	
	//Hier fehlt noch autoPrefix
	public void multiply(Value aValue) {
		//Normalisiere
		double v1=this.value*Math.pow(10, aValue.aPrefix.getExponent());
		double v2=this.value*Math.pow(10, this.aPrefix.getExponent());
		System.out.println(v1*v2);
		//Multipliziere
		setValue(v1*v2);
		//Vereinfache
		for(int i=1;i<9;i+=1) {
			if(this.value%(100*i)==0)
				System.out.println(i);
		}
			
		//setValue(this.value*aValue.value);
		setPrefix(this.aPrefix.multiply(aValue.aPrefix));
		setUnit(this.aUnit.multiply(aValue.aUnit));
	}
	
	public void divide(Value aValue) {
		setValue(this.value/aValue.value);
		setPrefix(this.aPrefix.divide(aValue.aPrefix));
		setUnit(this.aUnit.divide(aValue.aUnit));
	}
	
	public void add(Value Value) {
		if(Value.aUnit.equals(this.aUnit)) {
			setValue(this.value+Value.value);
		}else {
			throw new IncompatibleUnitsException();
		}
	}
	
	public void subtract(Value Value) {
		if(Value.aUnit.equals(this.aUnit)) {
			setValue(this.value-Value.value);
		}else {
			throw new IncompatibleUnitsException();
		}
	}
	
	
/*	
	add(Value) void
	substract(Value) void
	mulitiply(Value) void
	divide(Value)*/
	
	  public static void main(String[] args)
	  {
		  //ampere, kelvin, second, meter, kilogram;
		  //A^3 k^6 s^-5 m^5 und A^3 k^2 s^-1 m^4 kg^-4
		Unit u1 = new Unit(0,0,0,0,0);
		Prefix p1 = Prefix.Milli;
	    Value v1 = new Value(1, p1, u1);
	    
		Unit u2 = new Unit(0,0,0,0,0);
		Prefix p2 = Prefix.Giga;
	    Value v2 = new Value(1, p2, u2);
	    
	    v1.multiply(v2);
	    System.out.println(v1.toString());
		//Prefix a2 = Prefix.getPrefix(9);
		//System.out.println(a2.toString());
		//System.out.println(a1.multiply(a2));
	  }
	
}

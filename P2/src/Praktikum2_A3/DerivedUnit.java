package Praktikum2_A3;

enum DerivedUnit{
	Ampere(1,0,0,0,0,"A"),
	Kelvin(0,1,0,0,0,"K"),
	Sercond(0,0,1,0,0,"s"),
	Meter(0,0,0,1,0,"m"),
	Kilogram(0,0,0,0,1,"kg"),
	Newton(0,0,-2,1,1,"N"),
	Pascal(0,0,-2,-1,1,"Pa"),
	Joule(0,0,-2,2,1,"J"),
	Watt(0,0,-3,2,1,"W"),
	Coulomb(1,0,1,0,0,"C"),
	Volt(-1,0,-3,2,1,"V"),
	Farad(2,0,4,-2,-1,"F"),
	Ohm(-2,0,-3,2,1,"Ω"),
	Siemens(2,0,3,-2,-1,"S"),
	Weber(-1,0,-2,2,1,"Wb"),
	Tesla(-1,0,-2,0,1,"T"),
	Henry(-2,0,-2,2,1,"H");
	
	
	private final String symbol;
	private final int ampere, kelvin,second, meter, kilogram;
	private DerivedUnit(int ampere, int kelvin, int second, int meter, int kilogram, String symbol){
		this.symbol=symbol;
		this.ampere=ampere;
		this.kelvin=kelvin;
		this.second=second;
		this.meter=meter;
		this.kilogram=kilogram;
	}
	
	public String toString(){
		return symbol;
	}
	
	public Unit getBase() {
		return new Unit(ampere,kelvin,second,meter,kilogram);
	}
	
	  public static void main(String[] args)
	  {
	    DerivedUnit a1 = DerivedUnit.Ohm;
	    System.out.println(a1.toString());
	    System.out.println(a1.getBase());
	  }
	
}

package Praktikum2_A3;

public class Unit{
	int ampere, kelvin, second, meter, kilogram;
	public Unit(int ampere, int kelvin, int second, int meter, int kilogram){
		this.ampere=ampere;
		this.kelvin=kelvin;
		this.second=second;
		this.meter=meter;
		this.kilogram=kilogram;
	}
	public Unit(Unit aUnit){
		this(aUnit.ampere, aUnit.kelvin, aUnit.second, aUnit.meter, aUnit.kilogram);
	}
	
	public int getAmpere(){
		return ampere;
	}
	
	public void setAmpere(int ampere){
		this.ampere=ampere;
	}
	
	public int getKelvin(){
		return kelvin;
	}
	
	public void setKelvin(int kelvin){
		this.kelvin=kelvin;
	}
	
	public int getSecond(){
		return second;
	}
	
	public void setSecond(int second){
		this.second=second;
	}
	
	public int getMeter(){
		return meter;
	}
	
	public void setMeter(int meter){
		this.meter=meter;
	}
	
	public int getKilogram(){
		return kilogram;
	}
	
	public void setKilogram(int kilogram){
		this.kilogram=kilogram;
	}
	
	public Unit multiply(Unit aUnit){
		Unit nUnit = new Unit(aUnit.ampere+this.ampere, aUnit.kelvin+this.kelvin, aUnit.second+this.second, aUnit.meter+this.meter, aUnit.kilogram+this.kilogram);
		return nUnit;
	}
	
	public Unit divide(Unit aUnit){
		Unit nUnit = new Unit(this.ampere-aUnit.ampere, this.kelvin-aUnit.kelvin, this.second-aUnit.second, this.meter-aUnit.meter, this.kilogram-aUnit.kilogram);
		return nUnit;
	}
	
	public String toString(){
		for(DerivedUnit a : DerivedUnit.values()){
			if(this.equals(a.getBase())) {
				return a.toString();
			}
		}
		String nString = "";
		if(ampere!=0) {
			nString+="A^"+ampere;
		}
		if(kelvin!=0) {
			if(nString!="")
				nString+=" ";
			nString+="K^"+kelvin;
			}
		if(second!=0) {
			if(nString!="")
				nString+=" ";
			nString+="s^"+second;
		}
		if(meter!=0) {
			if(nString!="")
				nString+=" ";
			nString+="m^"+meter;
		}
		if(kilogram!=0) {
			if(nString!="")
				nString+=" ";
			nString+="kg^"+kilogram;
		}
		return nString;
	}

	public boolean equals2(Unit aUnit){
		if((ampere!=0 && aUnit.ampere==0) || (ampere==0 && aUnit.ampere!=0))
			return false;
		if((kelvin!=0 && aUnit.kelvin==0) || (kelvin==0 && aUnit.kelvin!=0))
			return false;
		if((second!=0 && aUnit.second==0) || (second==0 && aUnit.second!=0))
			return false;
		if((meter!=0 && aUnit.meter==0) || (meter==0 && aUnit.meter!=0))
			return false;
		if((kilogram!=0 && aUnit.kilogram==0) || (kilogram==0 && aUnit.kilogram!=0))
			return false;
		return true;
	}	
	
	public boolean equals(Unit aUnit){

		try {
			if(ampere == aUnit.ampere 
		    && kelvin == aUnit.kelvin
		    && second == aUnit.second
		    && meter == aUnit.meter
		    && kilogram == aUnit.kilogram) {
				return true;
			}else {
				return false;
			}
		}catch(Exception e) {
			return false;
		}
	}
			
		
	
	public static void main(String[] args){
		Unit a1 = new Unit(-2,0,-3,-2,1);
		//System.out.print(a1.getSekunde());
		//a1.toString()
		//System.out.println(a1.getSekunde());
		System.out.println(a1.toString());
		//DerivedUnit a2 = DerivedUnit.Ohm;
		//System.out.println(a2.toString());
		a1.equals(null);
		//System.out.println(a3.toString());
	}
}



package Praktikum2_A3;
import static org.junit.jupiter.api.Assertions.*;

import javax.xml.stream.events.Namespace;

import org.junit.jupiter.api.Test;

class DerivedUnitTest {

	
	final boolean contains(String aDerivedUnit) {
		for (DerivedUnit du : DerivedUnit.values()) {
			if (du.toString().equals(aDerivedUnit)) return true;
		}
		return false;
	}
	@Test
	final void testToString() {
		assertTrue(contains("A"),"Ampere fehlt");
		assertTrue(contains("K"),"Kelvin fehlt");
		assertTrue(contains("s"),"Second fehlt");
		assertTrue(contains("m"),"Meter fehlt");
		assertTrue(contains("kg"),"Kilogram fehlt");
		assertTrue(contains("N"),"Newton fehlt");
		assertTrue(contains("Pa"),"Pascal fehlt");
		assertTrue(contains("J"),"Joule fehlt");
		assertTrue(contains("W"),"Watt fehlt");
		assertTrue(contains("C"),"Coulomb fehlt");
		assertTrue(contains("V"),"Volt fehlt");
		assertTrue(contains("F"),"Farad fehlt");
		assertTrue(contains("Ω"),"Ohm fehlt");
		assertTrue(contains("S"),"Siemens fehlt");
		assertTrue(contains("Wb"),"Weber fehlt");
		assertTrue(contains("T"),"Tesla fehlt");
		assertTrue(contains("H"),"Henry fehlt");
		
	}

	@Test
	final void testGetBase() {
		String[] names = {"A","K","s","m","kg","N","Pa","J","W","C","V","F","Ω","S","Wb","T","H"};
		int[][] values = {{1,0,0,0,0},
				{0,1,0,0,0},
				{0,0,1,0,0},
				{0,0,0,1,0},
				{0,0,0,0,1},
				{0,0,-2,1,1},
				{0,0,-2,-1,1},
				{0,0,-2,2,1},
				{0,0,-3,2,1},
				{1,0,1,0,0},
				{-1,0,-3,2,1},
				{2,0,4,-2,-1},
				{-2,0,-3,2,1},
				{2,0,3,-2,-1},
				{-1,0,-2,2,1},
				{-1,0,-2,0,1},
				{-2,0,-2,2,1}
		};

		for (DerivedUnit du : DerivedUnit.values()) {
			for (int i=0; i<names.length;i++) {
				if (names[i].equals(du.toString())) {
					assertEquals(values[i][0],du.getBase().getAmpere(),"Ampere falsch bei "+du.name());
					assertEquals(values[i][1],du.getBase().getKelvin(),"Kelvin falsch bei "+du.name());
					assertEquals(values[i][2],du.getBase().getSecond(),"Second falsch bei "+du.name());
					assertEquals(values[i][3],du.getBase().getMeter(),"Meter falsch bei "+du.name());
					assertEquals(values[i][4],du.getBase().getKilogram(),"Kilogram falsch bei "+du.name());
				}
			}
			
			
		}

		
	}

}


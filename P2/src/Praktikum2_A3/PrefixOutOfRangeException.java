package Praktikum2_A3;

public class PrefixOutOfRangeException extends RuntimeException{
	private int exponent;
	
	public PrefixOutOfRangeException() {
	
	}
	
	public PrefixOutOfRangeException(int exponent){
		this.exponent=exponent;
	}
	
	public int getExponent() {
		return this.exponent;
	}
	
}

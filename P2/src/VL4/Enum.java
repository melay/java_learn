package VL4;



public class Enum {
	public enum SiPf {
		Giga, Mega, Kilo, ohne, milli, mikro, nano;
		
		public int expontent() {
			int exp = 9;
			exp = exp - this.ordinal()*3;
			return exp;
		}
	}
	
	public static void main(String []args){
		for(SiPf n : SiPf.values()) {
			System.out.println(n.name()+":"+n.expontent());
		}
	}
}

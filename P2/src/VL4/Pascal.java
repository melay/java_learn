package VL4;

public class Pascal {
	
	public long [] [] pd; 
	
	public void PdGen(){
		pd = new long[10][0];
		for(int i=0; i<pd.length;i++){
			pd[i]=new long[i+1];
			for(int j=0;j<pd[i].length;j++){
				if(j==0 || j==pd[i].length-1) {
					pd[i][j]=1;
				}else {
					pd[i][j]=pd[i-1][j-1]+pd[i-1][j];
				}
			}		
		}
	  }
	
	public void ausgabe() {
		for (long [] zeile : pd) {
			for (long feld : zeile) {
				System.out.print(feld+" ");
			}
			System.out.println();
		}
	}

	public static void main(String []args){
		Pascal pd = new Pascal();
	    pd.PdGen();
	    pd.ausgabe();
	 }
}


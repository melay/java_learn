package TTT.src;

import java.io.*; // Um von der Konsole einzulesen

public class TicTacToe extends Spiel {

    private int werIstDran;  // welcher Spieler
    
    private static BufferedReader stdin; // von der Konsole lesen
 
    public TicTacToe() {
	werIstDran=1; // Weiss faengt an
    }

    public Brett brett; 

    // Overriding der abstrakten Methoden
    public Brett neuesBrett() {
	brett = new Brett();
	return brett;
    }

    public void neuesSpiel(Brett b) {
	// Feld leeren: alles auf 0
	for (int i=0; i<9; i++) 
	    b.setFeld(i,0); 
    }


    public void macheZug(Brett b) {
	System.out.println(" Spieler " + werIstDran+", "+
			   "geben Sie einen Zug ein (0-8) :");

	Boolean fertig = false;
	do {
	    try {
		// Lies eine Zeile von der Konsole ein
		String line = stdin.readLine();
		// Umwandlung in Integerzahl
		int index = Integer.parseInt(line);
		
		if (index<9 && index>=0 && b.getFeld(index)==0) {
		    b.setFeld(index,werIstDran); // setzen
		    b.printBrett(); // ausgeben
		    fertig = true;
		    werIstDran = 3-werIstDran; // kleiner Trick ;-)
		}
		else { 
		    System.out.println("Wählen Sie ein anderes Feld");
		} 
	    }
	    // Das Einlesen und das Umwandeln kann Exceptions
	    // produzieren
	    catch (Exception ex) {
		System.out.println("Fehlerhafte Eingabe");
	    }
	    
	} while (!fertig);
    }

    public Boolean istBeendet(Brett b) {
	int anzahlLeereFelder=0;

	
	// Ist das Brett voll?
	for (int i=0; i<9; i++) 
	    if (b.getFeld(i)==0) anzahlLeereFelder++;
	// Wenn voll, dann zuende
	if (anzahlLeereFelder==0) return true;

	// Jetzt pruefen wir alle Moeglichkeiten,
	// dass das Spiel gewonnen wurde:

	// 2-dim. Array mit allen Gewinnkombinationen
	int kombination[][] = { {0,1,2}, // horizontal
				{3,4,5},
				{6,7,8},
				{0,3,6}, // vertikal
				{1,4,7},
				{2,5,8},
				{0,4,8}, // diagonal
				{2,4,6}};
	for (int i=0; i<kombination.length; i++) {
	    // hole die drei Werte
	    int v1 = b.getFeld(kombination[i][0]);
	    int v2 = b.getFeld(kombination[i][1]);
	    int v3 = b.getFeld(kombination[i][2]);
	    
	    // Sind die drei Werte alle gleich UND nicht leer?
	    if (v1==v2 && v2==v3 && v1!=0) return true;
	}
	/* Das haette man auch komplett anders machen koennen:
	   Schleifen, die die drei Horizontalen pruefen
	   Schleifen, die die drei Vertikalen pruefen
	   Zwei Test für die Diagonalen. 
	   Fuer TicTacToe ist diese "hart-kodierte" Variante
	   einfacher... 
	   
	   Fuer Vier-Gewinnt wuerde man zu den Schleifen 
	   greifen muessen
	*/
	   
	// nicht voll und noch kein Gewinner ==>
	// Spiel noch nicht zuende...
	return false;
    }
    
    public static void main(String[] args) {

	// Wir wollen von der Konsole lesen. Dazu muessen wir
	// in Java den "Eingabestrom" System.in in einen 
	// InputStreamReader auffangen, der wiederum von einem
	// gepufferten Reader gesteuert wird...
	stdin = new BufferedReader(new InputStreamReader(System.in)); 


	TicTacToe ttt = new TicTacToe();
	ttt.spielen();
	
    }
}

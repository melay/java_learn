package TTT.src;

public class Brett {
    private int[] feld;

    public Brett() {
	feld = new int[9];  // neues Array
    }
    
    // getter und setter
    public int getFeld(int pos) {
	return feld[pos];
    }
    public void setFeld(int pos, int wert) {
	feld[pos] = wert;
    }
    
    // Ausgabe des Brettes auf der Konsole
    // etwas aufgehuebscht
    public void printBrett() {
	System.out.println("+-+-+-+");
	for (int i=0; i<9; i++) {
	    System.out.print("|");
	    
	    switch(feld[i]) { 
	    case 0: System.out.print(" "); break;
	    case 1: System.out.print("o"); break;
	    case 2: System.out.print("x"); break;
	    default:  System.out.print("?");
	    }

	    if (i %3==2) { // neue Zeile?
		System.out.println("|");
		System.out.println("+-+-+-+");
	    }
	}
	System.out.println();
    }
}

package TTT.src;

public abstract class Spiel {
    public Brett brett;
    public abstract Brett neuesBrett();
    public abstract void neuesSpiel(Brett b);
    public abstract void macheZug(Brett b);
    public abstract Boolean istBeendet(Brett b);

    // Jetzt koennen wir schon spielen:
    public void spielen() {
	brett = neuesBrett();  
	neuesSpiel(brett);   // Grundaufstellung	
	do { // solange ziehen, bis beendet
	    macheZug(brett); 
	} while (! istBeendet(brett)); 
    }
}

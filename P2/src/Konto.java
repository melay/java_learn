public class Konto {
	private int kontonummer ;
	private double saldo ;

	public int getKontonummer () {
		return kontonummer ;
	}

	public void setKontonummer ( int nr) {
		kontonummer = nr;
	}

	public double getSaldo () {
		return saldo ;
	}

	public void setSaldo ( double betrag ) {
		saldo = betrag ;
	}

	public void zahleEin ( double betrag ) {
		saldo += betrag ;
	}

	public void zahleAus ( double betrag ) {
		saldo -= betrag ;
	}

	public void info () {
		System . out . println (" Kontonummer :" + kontonummer + " Saldo :" + saldo );
	}
 }
package VL7;
import java.io.*;
import java.util.*;

public class ArraySort {
	private static BufferedReader stdin; 
	//String line = stdin.readLine();
	
	
	public static void main(String[] args) {
		ArrayList<String> names = new ArrayList<String>();
		Boolean finish = false;
		stdin = new BufferedReader(new InputStreamReader(System.in)); 
		do{
			System.out.println("Gib einen Namen ein:");
			try{	
				String line = stdin.readLine();
				if(line.equals("finish")) {
					finish=true;
					break;
				}
				names.add(line);
			}catch (Exception ex) {
				System.out.println("Fehlerhafte Eingabe");
			}
			Collections.sort(names);
			System.out.println(names);
		}while(!finish);

	}
	
}
